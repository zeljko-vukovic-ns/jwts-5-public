﻿## Lab 8 - SPA, AngularJS

----

### $http servis

$http servis služi za komunikaciju sa HTTP serverom, u ovom slučaju REST serverom. Podržana je većina HTTP metoda, ali najčešće su get, put, post, delete.
Primeri korišćenja:

```javascript
$http.get('route') 									// poziva se GET route (route je putanja resursa ili kolekcije na REST servisu)
		.success(function(data, status) {			// ako je status 2xx (npr. 200 OK), u data se nalaze trazeni podaci
			// do something
		})
		.error(function(data, status) { 			// ako se dogodila greska, tj. ako status nije 2xx
			// handle error
		});
		
		
$http.post('route', { attrName : 'attrValue'})		// route je putanja kolekcije, drugi parametar je objekat koji se dodaje u kolekciju
		.success(...)
		.error(...)
		
$http.put('route', { attrName : 'attrValue'})		// route je putanja resursa, drugi parametar je objekat predstavlja izmenjen resurs
		.success(...)
		.error(...)
		
$http.delete('route')								// route je putanja resursa koji se briše
		.success(...)
		.error(...)
		
```

Za više informacija o $http servisu pogledati AngularJS dokumentaciju [https://docs.angularjs.org/api/ng/service/$http](https://docs.angularjs.org/api/ng/service/$http)

----

### $location servis

$location servis služi za programsko čitanje i menjanje URL adrese u address baru browsera.
Primer korišćenja:

```javascript
$location.path('/route'); 		// redirekcija korisnika na route stranicu
```

Za više informacija o $location servisu pogledati AngularJS dokumentaciju [https://docs.angularjs.org/api/ng/service/$location](https://docs.angularjs.org/api/ng/service/$location)

----

* Implementacija izmene aktivnosti - dodati novo dugme Edit za svaku aktivnost u koloni Actions u tabeli za prikaz svih aktivnosti.
Klikom na ovo dugme korisnik se usmerava na stranicu /#/activities/edit/:id (:id predstavlja id aktivnosti). Dodati potrebnu rutu za izmenu aktivnosti.

* Koristiti $routeParams servis u **ActivitiesController** za dobavljanje parametara rute - u ovom slučaju id aktivnosti.
Iskoristiti id aktivnosti za učitavanje aktivnosti sa servera i popunjavanje forme za izmenu aktivnosti.

----

* Implementacija pretraživanja aktivnosti po imenu - dodati input polje u koje će korisnik unositi upit pretrage, kao i dugme Search.

* Implementirati pretraživanje korišćenjem AngularJS filtera (client-side).

* Implementirati pretraživanje slanjem upita na REST servis (server-side).

----

#### Domaći zadatak

* Na stranici za prikaz svih aktivnosti dodati checkbox. Kada se pritisne dugme Search, ako je 
ovaj checkbox čekiran, aktivnosti sortirati po ID u opadajućem redosledu, a ako nije čekiran,
aktivnosti sortirati po ID u rastućem redosledu.