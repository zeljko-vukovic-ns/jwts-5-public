package jwts.wafepa.repository;

import java.util.List;

import jwts.wafepa.model.Activity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ActivityRepository extends JpaRepository<Activity, Long> {

//	@Query("select a from Activity a where a.name = :name")
//	Activity findByName(@Param("name") String name);
	
	List<Activity> findByName(String name);
	
	@Query("select a from Activity a order by a.id asc")
	List<Activity> findAllOrderByIdAsc();
	
	@Query("select a from Activity a order by a.id desc")
	List<Activity> findAllOrderByIdDesc();
}
