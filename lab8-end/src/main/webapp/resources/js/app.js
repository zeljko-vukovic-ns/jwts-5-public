var wafepaApp = angular.module('wafepaApp', ['ngRoute']);

wafepaApp.controller('ActivitiesController', function($scope, $http, $location, $routeParams) {
	
	$scope.getActivities = function() {
		
		var parameters = {};
		if ($scope.search) {
			parameters = { name : $scope.search };
		}
		
		$http.get('api/activities', { params : parameters })
			.success(function(data) {
				$scope.activities = data;
				$scope.successMessage = 'Everything is fine.';
			})
			.error(function() {
				$scope.errorMessage = 'Oops, something went wrong.';
			});
	};
	
	$scope.deleteActivity = function(id, index) {
		$http.delete('api/activities/' + id)
			.success(function (data) {
				$scope.activities.splice(index, 1);
			});
	};
	
	$scope.initActivity = function() {
		$scope.activity = {};
		
		if ($routeParams && $routeParams.id) {
			$http.get('api/activities/' + $routeParams.id)
				.success(function(data) {
					$scope.activity = data;
				});
		}
	};
	
	$scope.saveActivity = function() {
		if ($scope.activity.id) {
			$http.put('api/activities/' + $scope.activity.id, $scope.activity)
				.success(function(data) {
					$location.path('/activities');
				});
		} else {
			$http.post('api/activities', $scope.activity)
			.success(function(data) {
				$location.path('/activities');
			});
		}
	};
});

wafepaApp.config([ '$routeProvider', function($routeProvider) {
	
	$routeProvider
		.when('/', { 
			templateUrl : 'resources/html/home.html'
		})
		.when('/activities', {
			templateUrl : 'resources/html/activities.html',
			controller : 'ActivitiesController'
		})
		.when('/activities/add', {
			templateUrl : 'resources/html/addEditActivity.html',
			controller : 'ActivitiesController'
		})
		.when('/activities/edit/:id', {
			templateUrl : 'resources/html/addEditActivity.html',
			controller : 'ActivitiesController'
		})
		.otherwise({
			redirectTo : '/'
		})
		;
}]);