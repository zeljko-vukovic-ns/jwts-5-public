var wafepaApp = angular.module('wafepaApp.controllers', []);

wafepaApp.controller('ActivitiesController', function($scope, $location, $routeParams, activitiesRestService) {
	
	$scope.page = 0;
	
	$scope.getActivities = function() {
		
		var parameters = { page : $scope.page };
		
		if ($scope.search) {
			parameters.name = $scope.search;
		}
		
		activitiesRestService.getActivities(parameters)
			.success(function(data) {
				$scope.activities = data;
				$scope.successMessage = 'Everything is fine.';
			})
			.error(function() {
				$scope.errorMessage = 'Oops, something went wrong.';
			});
	};
	
	$scope.deleteActivity = function(id, index) {
		activitiesRestService.deleteActivity(id)
			.success(function (data) {
				$scope.activities.splice(index, 1);
			});
	};
	
	$scope.initActivity = function() {
		$scope.activity = {};
		
		if ($routeParams && $routeParams.id) {
			activitiesRestService.getActivity($routeParams.id)
				.success(function(data) {
					$scope.activity = data;
				});
		}
	};
	
	$scope.saveActivity = function() {
		activitiesRestService.saveActivity($scope.activity)
			.success(function(data) {
				$location.path('/activities');
			})
	};
	
	$scope.changePage = function(page) {
		$scope.page = page;
		$scope.getActivities();
	};
});