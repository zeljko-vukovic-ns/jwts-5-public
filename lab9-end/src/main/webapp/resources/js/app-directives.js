var wafepaApp = angular.module('wafepaApp.directives', []);

wafepaApp.directive('activitiesTable', function() {
	return {
		restrict : 'E',
		templateUrl : 'resources/html/activitiesTable.html',
		controller : 'ActivitiesController'
	}
});