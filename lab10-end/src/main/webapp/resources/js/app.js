var wafepaApp = angular.module('wafepaApp', ['ngRoute',
                                             'pascalprecht.translate',
                                             'wafepaApp.controllers',
                                             'wafepaApp.directives',
                                             'wafepaApp.services']);

wafepaApp.config([ '$routeProvider', function($routeProvider) {
	
	$routeProvider
		.when('/', { 
			templateUrl : 'resources/html/home.html'
		})
		.when('/activities', {
			templateUrl : 'resources/html/activities.html',
			controller : 'ActivitiesController'
		})
		.when('/activities/add', {
			templateUrl : 'resources/html/addEditActivity.html',
			controller : 'ActivitiesController'
		})
		.when('/activities/edit/:id', {
			templateUrl : 'resources/html/addEditActivity.html',
			controller : 'ActivitiesController'
		})
		.otherwise({
			redirectTo : '/'
		})
		;
}]);

wafepaApp.config([ '$translateProvider' , function($translateProvider) {
	
	$translateProvider.translations('en', {
		'NAME' : 'Name',
		'ACTIONS' : 'Actions'
	});
	
	$translateProvider.translations('sr', {
		'NAME' : 'Ime',
		'ACTIONS' : 'Akcije'
	});
	
	var lang = localStorage.getItem('lang');
	if (lang) {
		$translateProvider.preferredLanguage(lang);
	}
	else {
		$translateProvider.preferredLanguage('en');
	}
}]);