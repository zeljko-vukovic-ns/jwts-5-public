package jwts.wafepa.web.controller;

import java.util.ArrayList;
import java.util.List;

import jwts.wafepa.model.Activity;
import jwts.wafepa.service.ActivityService;
import jwts.wafepa.web.dto.ActivityDTO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/activities")
public class ApiActivityController {

	@Autowired
	ActivityService activityService;
	
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<List<ActivityDTO>> getActivities(){
		List<Activity> activities = activityService.findAll();
		if(activities==null){
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		List<ActivityDTO> activitiesDTO = new ArrayList<>();
		for(Activity activity : activities){
			ActivityDTO activityDTO = new ActivityDTO(activity);
			activitiesDTO.add(activityDTO);
		}
		return new ResponseEntity<>(activitiesDTO, HttpStatus.OK);
	}
	
	@RequestMapping(value="/{id}")
	public ResponseEntity<ActivityDTO> getActivity(@PathVariable Long id){
		Activity activity = activityService.findOne(id);
		if(activity == null){
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(new ActivityDTO(activity), HttpStatus.OK); 
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<ActivityDTO> deleteAcivity(@PathVariable Long id){
		Activity activity = activityService.findOne(id);
		if(activity == null){
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		activityService.remove(id);
		return new ResponseEntity<>(new ActivityDTO(activity), HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.PUT, value="/{id}")
	public ResponseEntity<ActivityDTO> editActivity(@PathVariable Long id,
			@RequestBody ActivityDTO activityDTO){
		Activity activity = activityService.findOne(id);
		HttpStatus status = HttpStatus.OK;
		if(activity == null){
			activity = new Activity();
			status = HttpStatus.CREATED;
			if(activityDTO.getId()!=id && activityDTO.getId()!=null){
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST); 
			}
		}else{
			if(activity.getId()!=id){
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST); 
			}
		}
		activity.setId(activityDTO.getId());
		activity.setName(activityDTO.getName());
		activity = activityService.save(activity);
		return new ResponseEntity<>(new ActivityDTO(activity), status);
	}
	
	
	
	@RequestMapping(method=RequestMethod.POST, consumes="application/json")
	public ResponseEntity<ActivityDTO> saveActivity(
			@RequestBody ActivityDTO activityDTO){
		
		Activity activity = new Activity();
		activity.setId(activityDTO.getId());
		activity.setName(activityDTO.getName());
		activity = activityService.save(activity);
		
		return new ResponseEntity<ActivityDTO>(new ActivityDTO(activity),HttpStatus.CREATED);
		
	}
}
