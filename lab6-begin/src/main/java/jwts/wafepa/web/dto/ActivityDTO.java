package jwts.wafepa.web.dto;

import jwts.wafepa.model.Activity;

public class ActivityDTO {
	
	private String activityName;
	private Long id;
	
	
	
	public ActivityDTO() {
		super();
	}

	public ActivityDTO(Activity activity){
		super();
		
		this.id = activity.getId();
		this.activityName = activity.getName();
	}

	public String getActivityName() {
		return activityName;
	}

	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	

	
	
}
