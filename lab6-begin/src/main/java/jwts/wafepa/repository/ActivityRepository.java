package jwts.wafepa.repository;

import jwts.wafepa.model.Activity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ActivityRepository extends JpaRepository<Activity, Long> {

//	@Query("select a from Activity a where a.name = :name")
//	Activity findByName(@Param("name") String name);
	
	Activity findByName(String name);
}
